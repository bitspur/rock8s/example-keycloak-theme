group "default" {
  targets = [
    "default"
  ]
}

target "base" {
  context    = ".."
  dockerfile = "Dockerfile"
  platforms  = ["linux/amd64"]
  tags = [
    "${REGISTRY}:${VERSION}"
  ]
}
