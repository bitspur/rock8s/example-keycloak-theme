# File: /Makefile
# Project: keycloak-account-avatar
# File Created: 05-06-2024 06:31:41
# Author: Clay Risser
# -----
# Last Modified: 05-06-2024 06:43:18
# Modified By: Clay Risser
# -----
# Risser Labs LLC (c) Copyright 2022 - 2024

.ONESHELL:
.POSIX:
.SILENT:
.DEFAULT_GOAL := default
MKPM := ./mkpm
.PHONY: default
default:
	@$(MKPM) $(ARGS)
.PHONY: %
%:
	@$(MKPM) "$@" $(ARGS)
